\select@language {english}
\select@language {english}
\contentsline {section}{Remerciements}{3}{section*.2}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{Introduction}{4}{section.1}
\contentsline {subsection}{Internship proposal}{4}{section*.3}
\contentsline {subsection}{The Company}{5}{section*.4}
\contentsline {subsubsection}{Siemens Businesses}{5}{section*.5}
\contentsline {subsubsection}{History}{5}{section*.6}
\contentsline {subsubsection}{Managing board}{7}{section*.7}
\contentsline {subsection}{Company's level of maturity on internship proposal}{8}{section*.8}
\contentsline {subsection}{Knowledge on intership proposal}{8}{section*.9}
\contentsline {section}{\numberline {2}Organizational dimension}{9}{section.2}
\contentsline {section}{\numberline {3}Scientist Aspect}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Initial situation and goals}{10}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Overview}{10}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}CPU}{10}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Rack and I/O section}{10}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Programming PLCs}{11}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Real-Time Embedded Systems}{11}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Goals}{12}{subsubsection.3.1.6}
\contentsline {subsection}{\numberline {3.2}Trace}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Jobs}{13}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Data Address}{14}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Tracing Data}{15}{subsubsection.3.2.3}
\contentsline {paragraph}{Trace Job}{15}{section*.10}
\contentsline {paragraph}{Read Trace Job}{16}{section*.11}
\contentsline {paragraph}{Large buffer}{17}{section*.12}
\contentsline {subsubsection}{\numberline {3.2.4}How this works ?}{17}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}Compression algorithm}{17}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}The Compression}{17}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}The Algorithm}{18}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}The Results}{19}{subsubsection.3.3.3}
\contentsline {section}{Conclusion}{23}{section*.13}
\contentsline {section}{R\IeC {\'e}f\IeC {\'e}rences}{24}{section*.14}
